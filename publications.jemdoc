# jemdoc: menu{MENU}{publications.html}
= Publications

Google Scholar Link:
[https://scholar.google.com/citations?user=iks1GnkAAAAJ&hl=en&oi=ao Haricharan Lakshman]

== Book
- H. Lakshman,\n
*Interpolation for Coding and Upsampling of Video Signals*\n
Mensch und Buch Verlag, 2014, ISBN: 978-3-86387-431-5

== Book Chapter
- B. Bross, P. Helle, H. Lakshman, K. Ugur,\n 
*Inter-Picture Prediction in HEVC* \[[pdfs/2014_hevc_alg.pdf pdf]\]\n
part of “High Efficiency Video Coding (HEVC) - Algorithms and Architectures,”\n 
Integrated Circuits and Systems Series, Springer, June 2014.


== Journal Publications
- H. Lakshman, W.-Q Lim, H. Schwarz, D. Marpe, G. Kutyniok, T. Wiegand,\n 
*Image Interpolation using Shearlet based iterative refinement* \[[pdfs/2015_IC.pdf pdf]\]\n 
Elsevier Signal Processing: Image Communication, vol. 36, pp. 83 - 94, August 2015.


- H. Lakshman, H. Schwarz, T. Wiegand,\n 
*Generalized Interpolation Based Fractional Sample Motion Compensation* \[[pdfs/2013_CSVT.pdf pdf]\]\n 
IEEE Transactions on Circuits and Systems for Video Technology, vol. 23, no. 3, pp. 455 - 466, March 2013.


- K. Mueller, H. Schwarz, D. Marpe, C. Bartnik, S. Bosse, H. Brust, T. Hinz, H. Lakshman, P. Merkle, H. F. Rhee, G. Tech, M. Winken, and T. Wiegand,\n *3D High Efficiency Video Coding for Multi-View Video and Depth Data* \[[pdfs/2013_TIP.pdf pdf]\]\n 
IEEE Transactions on Image Processing, Special Issue on 3D Video Representation, vol. 22, no. 9, pp. 3366 - 3378, September 2013.


- P. Ndjiki-Nya, M. Koeppel, H. Lakshman, P. Merkle, K. Mueller, T. Wiegand,\n 
*Depth Image-based Rendering with Advanced Texture Synthesis for 3D Video* \[[pdfs/2011_TMM.pdf pdf]\]\n 
Transactions on Multimedia, June 2011.\n
+2013 IEEE Communications Society MMTC Best Journal Paper Award+


- D. Marpe, H. Schwarz, S. Bosse, B. Bross, P. Helle, T. Hinz, H. Kirchhoffer,\n 
H. Lakshman, T. Nguyen, S. Oudin, M. Siekmann, K. Suehring, M. Winken, and T. Wiegand,\n 
*Video Compression Using Quadtrees, Leaf Merging and \n
Novel Techniques for Motion Representation and Entropy Coding* \[[pdfs/2010_CSVT.pdf pdf]\]\n 
IEEE Trans. on Circuits and Systems for Video Technology, December 2010.\n
+Invited Paper+


== Conference Publications
- J. Thatte, J.-B. Boin, H. Lakshman, G. Wetzstein, B. Girod,\n
*Depth Augmented Stereo Panorama for Cinematic Virtual Reality with Focus Cues* \[[pdfs/2016_ICIP_1.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Phoenix, USA, September 2016.\n
+Conference Best Paper Award for Industry+

- A. Araujo, H. Lakshman, R. Angst and B. Girod,\n 
*Modeling the Impact of Keypoint Detection Errors on Local Descriptor Similarity* \[[pdfs/2016_ICIP_2.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Phoenix, USA, September 2016.\n

- J. Thatte, J.-B. Boin, H. Lakshman, B. Girod,\n
*Depth Augmented Stereo Panorama for Cinematic Virtual Reality with Head Motion Parallax* \[[pdfs/2016_ICME.pdf pdf]\]\n 
IEEE International Conference on Multimedia and Expo, Seattle, USA, July 2016.\n

- M. Yu, H. Lakshman, B. Girod,\n 
*Content Adaptive Representations of Omnidirectional Videos for Cinematic Virtual Reality* \[[pdfs/2015_ACMMM.pdf pdf]\]\n 
ACM Multimedia Workshop on Immersive Media Experiences, Brisbane, Australia, November 2015.\n


- M. Yu, H. Lakshman, B. Girod,\n 
*A Framework to Evaluate Omnidirectional Video Coding Schemes* \[[pdfs/2015_ISMAR.pdf pdf]\]\n 
IEEE International Symposium on Mixed and Augmented Reality, Fukuoka, Japan, September 2015.\n


- M. Farre, H. Lakshman, P. Helle, H. Schwarz, K. Mueller, D. Marpe and T. Wiegand,\n
*Edge aware disparity estimation for intermediate view synthesis* \[[pdfs/2013_PCS.pdf pdf]\]\n
Picture Coding Symposium, San Jose, December 2013.


- H. Lakshman, W.-Q Lim, H. Schwarz, D. Marpe, G. Kutyniok, T. Wiegand,\n 
*Image Interpolation using Shearlet based Sparsity Priors* \[[pdfs/2013_ICIP.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Melbourne, Australia, September 2013. \n 


- T. Hinz, P. Helle, H. Lakshman, M. Siekmann, J. Stegemann, H. Schwarz, D. Marpe, T, Wiegand,\n
*An HEVC extension for spatial and quality scalable video coding* \[[pdfs/2013_SPIE.pdf pdf]\]\n
Proc. SPIE 8666, Visual Information Processing and Communication IV, February 2013.


- P. Helle, H. Lakshman, M. Siekmann, J. Stegemann, T. Hinz, H. Schwarz, D. Marpe, and T. Wiegand,\n
*A Scalable Video Coding Extension of HEVC* \[[pdfs/2013_DCC.pdf pdf]\]\n 
Data Compression Conference, Snowbird, Utah, March 2013.


- A. F. de Araujo, F. Silveira, H. Lakshman, J. Zepeda, A. Sheth, P. Perez and B. Girod,\n 
*The Stanford\/Technicolor\/Fraunhofer HHI Video Semantic Indexing* \[[pdfs/2012_TRECVID.pdf pdf]\]\n 
TRECVID2012 Workshop, November 2012.


- M. Makar, H. Lakshman, V. Chandrasekhar and B. Girod,\n 
*Gradient Preserving Quantization* \[[pdfs/2012_ICIP.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Orlando, USA, September 2012.\n 
+Best student paper award finalist+


- H. Schwarz, C. Bartnik, S. Bosse, H. Brust, T. Hinz, H. Lakshman,\n 
D. Marpe, P. Merkle, K. Mueller, H. Rhee, G. Tech, M. Winken, and T. Wiegand,\n
*3D Video Coding Using Advanced Prediction, Depth Modeling, and Encoder Control Methods* \[[pdfs/2012_PCS_3D.pdf pdf]\]\n
Picture Coding Symposium, Krakow, Poland, May 2012.


- H. Lakshman, C. Rudat, M. Albrecht, H. Schwarz, D. Marpe, and T. Wiegand,\n 
*Conditional Motion Vector Refinement for Improved Prediction* \[[pdfs/2012_PCS_CMV.pdf pdf]\]\n
Picture Coding Symposium, Krakow, Poland, May 2012.


- H. Lakshman, H. Schwarz, T. Blu, and T. Wiegand,\n 
*Generalized Interpolation for Motion Compensated Prediction* \[[pdfs/2011_ICIP.pdf pdf]\]\n
IEEE International Conference on Image Processing, September 2011.\n
+Invited Paper+


- D. Marpe, H. Schwarz, S. Bosse, B. Bross, P. Helle, T. Hinz, H. Kirchhoffer,\n 
H. Lakshman, T. Nguyen, S. Oudin, M. Siekmann, K. Suehring, M. Winken, and T. Wiegand,\n 
*Improved Video Compression Technology and the Emerging High Efficiency Video Coding Standard* \[[pdfs/2011_ICCE.pdf pdf]\]\n 
ICCE Berlin, Germany, September 2011.


- H. Lakshman, B. Bross, H. Schwarz, and T. Wiegand,\n 
*Fractional-Sample Motion Compensation using Generalized Interpolation* \[[pdfs/2010_PCS_MCP.pdf pdf]\]\n 
Picture Coding Symposium, Nagoya, Japan, December 2010.


- D. Marpe, H. Schwarz, S. Bosse, B. Bross, P. Helle, T. Hinz, H. Kirchhoffer,\n 
H. Lakshman, T. Nguyen, S. Oudin, M. Siekmann, K. Suehring, M. Winken, and T. Wiegand,\n 
*Highly Efficient Video Compression Using Quadtree Structures and Improved Techniques for Motion Representation and Entropy Coding* \[[pdfs/2010_PCS_VC.pdf pdf]\]\n 
Picture Coding Symposium, Nagoya, Japan, December 2010.


- P. Ndjiki-Nya, D. Doshkov, H. Lakshman, M. Koeppel, and T. Wiegand,\n
*Towards Efficient Intra Prediction based on Image Inpainting Methods* \[[pdfs/2010_PCS_INP.pdf pdf]\]\n
Picture Coding Symposium, Nagoya, Japan, December 2010.


- H. Lakshman, H. Schwarz, and T. Wiegand,\n 
*Adaptive Motion Model Selection using a Cubic Spline based Estimation Framework* \[[pdfs/2010_ICIP.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Hong Kong, September 2010.\n
+Best student paper award finalist+


- M. Koeppel, P. Ndjiki-Nya, D. Doshkov, H. Lakshman, P. Merkle, K. Mueller, and T. Wiegand,\n 
*Temporally Consistent Handling of Disocclusions with Texture Synthesis for Depth Image based Rendering* \[[pdfs/2010_ICIP_INP.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Hong Kong, China, September 2010.


- P. Ndjiki-Nya, M. Koeppel, D. Doshkov, H. Lakshman, P. Merkle, K. Mueller, and T. Wiegand,\n 
*Depth Image based Rendering with Advanced Texture Synthesis* \[[pdfs/2010_ICME.pdf pdf]\]\n 
IEEE International Conference on Multimedia \& Expo, Singapore, July 2010.


- H. Lakshman, H. Schwarz, and T. Wiegand,\n 
*Video Coding with Cubic Spline Interpolation and Adaptive Motion Model Selection* \[[pdfs/2010_SPCOM.pdf pdf]\]\n 
IEEE International Conference on Signal Processing and Communications, Bangalore, India, July 2010.


- H. Lakshman, M. Koeppel, P. Ndjiki-Nya, and T. Wiegand,\n 
*Image Recovery using Sparse Reconstruction based Texture Refinement* \[[pdfs/2010_ICASSP.pdf pdf]\]\n 
IEEE International Conference on Acoustics, Speech, and Signal Processing, Dallas, TX, USA, March 2010.


- H. Lakshman, P. Ndjiki-Nya, M. Koeppel, D. Doshkov, and T. Wiegand,\n 
*An Automatic Structure-Aware Image Extrapolation Applied to Error Concealment* \[[pdfs/2009_ICIP.pdf pdf]\]\n 
IEEE International Conference on Image Processing, Cairo, Egypt, November 2009.


- J. Seiler, H. Lakshman, A. Kaup,\n 
*Spatio-Temporal Prediction in Video Coding by Best Approximation* \[[pdfs/2009_PCS.pdf pdf]\]\n 
Picture Coding Symposium, pp. 81-84, Chicago, USA, May 2009.
